package org.uplgenielogiciel.uplgenielogiciel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UplGenielogicielApplication {

	public static void main(String[] args) {
		SpringApplication.run(UplGenielogicielApplication.class, args);
	}

}
